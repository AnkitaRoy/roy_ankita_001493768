/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ankitaroy
 */
public class AssignmentLab1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
        Person p=new Person();
        p.setFirstName("Ankita");
        p.setLastName("Roy");
        String sDate1="20/11/1992";
        
        Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        
        p.setDateOfBirth(date1);
       
        
        
        p.setHomeAddress(new Address());
        p.setLocalAddress(new Address());
        p.setWorkAddress(new Address());
        Address homeAddress = p.getHomeAddress();
        homeAddress.setCity("kolkata");
        homeAddress.setCountry("India");
        homeAddress.setState("West Bengal");
        homeAddress.setStreetLine1("31/s");
        Address localAddress = p.getLocalAddress();
        localAddress.setCity("Park drive");
        localAddress.setCountry("India");
        localAddress.setState("Maharastra");
        localAddress.setStreetLine1("jjjj");
        localAddress.setStreetLine2("uuuu");
        localAddress.setZipcode(12345);
        Address workAddress=p.getWorkAddress();
        workAddress.setCity("boston");
        workAddress.setCountry("usa");
        workAddress.setState("mas");
        workAddress.setStreetLine1("75 peterborough");
        workAddress.setStreetLine2("413");
        workAddress.setZipcode(02215);
        System.out.println("firstname:"+p.getFirstName()+"\t|\tlastname:"+p.getLastName());
        System.out.println(""+p.getDateOfBirth());
      
        System.out.println();
       } catch (ParseException ex) {
           System.out.println(ex);
        } 
        
    }
    
}
